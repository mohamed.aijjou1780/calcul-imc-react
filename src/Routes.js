import React, { Component } from "react";
import { Switch, Router, Route } from "react-router-dom";
import Creation from "./Creation";
import Identification from "./Identification";
import Home from "./Home";
import AffichageMois from "./Affichage-mois"
import AffichageTri from "./Affichage-tri"
import history from "./history";
import FormPoids from "./FormPoids";
export class Routes extends Component {
  constructor(props) {
    super(props);
    this.stat = {};
  }

  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" exact>
            <Identification></Identification>
          </Route>
          <Route path="/Form">
            <FormPoids></FormPoids>
          </Route>
          <Route path="/Home">
            <Home></Home>
          </Route>
          <Route path="/Creation">
            <Creation></Creation>
          </Route>
          <Route path="/Tri">
            <AffichageTri></AffichageTri>
          </Route>
          <Route path="/Mois">
            <AffichageMois></AffichageMois>
          </Route>
        </Switch>
      </Router>
    );
  }
}
